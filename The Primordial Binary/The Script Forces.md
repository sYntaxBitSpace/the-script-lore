### The Script Forces ###

The script forces are the primordial form of scripts. They are the collective in which all forms of code and scripts are connected and coexist in peace without any 
intervention of outside factors. The script force may be represented as the formation of waves of code, inside the primordial Binary. Not many is known about them,
except that they are forces of imense power.

### The Origins of Script Forces ###

The origins of the Script Forces are still unknown, not even to the script Gods. Many Script Monks proclaimed that the script Forces came out of the Duality, as it
imploded upon itself. They claim that the pulse of the Duality forced the script Forces into existence, yet that theory is yet to be proved. Another theory is that
the Duality was formed by a reduced state of the Script Forces, and they existed even before the Duality.

Although the script Forces have such a mysterous origin, many Script Monks and Masters have spent years of studying their secrets. Even the one of two Coders Supreme
Phill stated: *"The script Forces are waves of binary energy flowing through reality itself, shaping it. Although they might seem as deities, they are not, for their
actions are not controled by themselves, but rather dictated by the Duality. They connect everything and everyone, and set in stone the rules of reality as it appears
to our mortal eyes."*

### The Qualities of Script Force ###

"One who controls the flows of the Script Force, controls reality itself" -Karl Steinbuch, Great Script Lord of Neural Networks

The Script Force, at it's base is controlable only by deities called The Script Titans.
The Script Titans are a force of shaping, an ordering kind of deities, that caught shape in the forming of the universe.

"01111001 01100101 01110100 00100000 01100100 01100101 01100011 01101001 01110000 01101000 01100101 01110010 01100101 01100100 00101100 00100000 01100001 01100010 01101000 01110011
01100010 01100100 01101011 01101010 01100111 01101000 01110110 01100001 01100111 01110011 01110110 01101011 01101110 
01100001 01101100 01101000 01110111 01110110 01100100 01101010 01101011 01101000 01101110 01100010 01100001 01101100 01110011 01101000
01110110 01100100 01101010 01100001 01110011 01110110 01100010 01100100 01101010 01100111 01100001 01110110 01101100 01110011 01101000 01100100 01100010 01101100
01100001 01110011 01101000 01110110 01100100 01100001 01110011 01100100 01100001 01110011 01100100 00100000 01100001 
01110011 01100100 01100001 01100010 01110011 01101100 01100100 01101000 01100010 01100001 00100000 00111011 01101010 00110011 00110010 00110001 00100000 01101011 
01100001 01100010 01110011 01100100 01100001 01101110 01110111 01100100 01100010 01100001 01101100 01101000 01110111 01110011 01100010 01100100 00100000 01100001 01101000
01110111 01100010 01100100 01101100 01100001 01101011 01101000 01100010 01110111 01101000 01100010 01101100 01101000 01100001 01100010 01110111 01110110
01100001 01110000 00111011 01101111 01110111 01101110 
01101101 01100100 00111011
01100001 01101010 0110001001101110 01110111 01101001 01100100 01100010 01100001
01101100 01101011 01110111 01110011 01100010 01100100 " - Piece of code, encrypted in the Primordial Binary.


### Known Script Forces ###

## Stack Overflow - The collective ##

Stack Overflow is the Script Force of collective Script Knoledge. It posesses in it every Code ever written. Script Lords Call upon this force to recieve wisdom and knoledge about the 
ways of Scripts. Not all Script Lords can call upon the Script Force of Stack Overflow, but those who can, possess power beyond human comprehension.

## 01100001 01101000 01101010 01100010 01110011 01101010 01101011 01101000 01100100 01100010 01101000 01100001  ##

01100100 01101000 01100010 01100001 00100000 00111011 01101010 00110011 00110010 00110001 00100000 01101011 
01100001 01100010 01110011 01100100 01100001 01101110 01110111 01100100 01100010 01100001 01101100 01101000 01110111 01110011 01100010 01100100 00100000 01100001 01101000
01110111 01100010 01100100 01101100 01100001 01101011 01101000 01100010 01110111 01101000 01100010 01101100 01101000 01100001 01100010 01110111 01110110




